let myHonda = {
  color: 'red',
  wheels: 4,
  bodyStyle: {
    door: 5,
    type: 'hatchback'
  },
  engine: {
    type: 'R',
    cylinders: 4,
    volume: 2.2
  },
  transmission: {
    speed: 6,
    type: 'manual'
  },
  passangers: ['I', 'he', 'she']
};

let clone = (value) => JSON.parse(JSON.stringify(value))

let hisHonda = clone(myHonda);

hisHonda.color = 'green';
hisHonda.bodyStyle.type = 'sedan';
hisHonda.bodyStyle.door = 4;
hisHonda.transmission.type = 'automatic';

console.log('myHonda ', myHonda);
console.log('hisHonda ', hisHonda);