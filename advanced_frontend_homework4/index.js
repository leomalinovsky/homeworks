'use strict'
/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/

class Hamburger {
	constructor (size, stuffing) {
		if (arguments.length === 0) {
			this.hamburgerException('Error! No arguments');
		} else if (arguments.length === 1) {
			this.hamburgerException('Error! Should be two arguments'); 
		} else if (typeof size != 'object' || typeof stuffing != 'object') {
			this.hamburgerException('size or/and stuffing is not an object'); 
		} else {
			this._size = size;
			this._stuffing = stuffing;
			this._toppings = [];
		}
	}

	hamburgerException (message) {
   	return console.log(message);
	}

	set toppings (topping) {
		try {
			if (!this._toppings.includes(topping)) {	
				this._toppings.push(topping);	
			} else if (this._toppings.includes(topping)) {
				this.hamburgerException('Error! '+topping.name+' is already added');
			}
		}
		catch (err) {
			console.log(err);
		}
	}
	
	removeTopping (topping) {
		try {
			if (this._toppings.length === 0) {
				this.hamburgerException('Error! There is no topping');
			} else if (this._toppings.includes(topping)) {
				this._toppings = this._toppings.filter(function(item){
					return item.name != topping.name;
				})
			}
		}
		catch (err) {
			console.log(err);
		}
	}

	get toppings () {
		return this._toppings;
	}

	get size () {
		return this._size;
	}

	get stuffing () {
		return this._stuffing;
	}

	calculatePrice () {
		let price = this._size.price + this._stuffing.price;
		this._toppings.forEach( item => {
			price += item.price;
		})
		return price;  
	}

	calculateCalories () {
		let calories = this._size.calories + this._stuffing.calories;
		this._toppings.forEach( item => {
			calories += item.calories;
		})
		return calories;
	}

}

/* Размеры, виды начинок и добавок */
const SIZE_SMALL = {
	value: 'small',
	price: 50,
	calories: 20
}
const SIZE_LARGE = {
	value: 'large',
	price: 100,
	calories: 40
}
const STUFFING_CHEESE = {
	name: 'cheese',
	price: 10,
	calories: 20
}
const STUFFING_SALAD = {
	name: 'salad',
	price: 20,
	calories: 5
}
const STUFFING_POTATO = {
	name: 'potato',
	price: 15,
	calories: 10
}
const TOPPING_MAYO = {
	name: 'mayonnaise',
	price: 20,
	calories: 5
}
const TOPPING_SPICE = {
	name: 'spice',
	price: 15,
	calories: 0
}

const hamburger = new Hamburger(SIZE_LARGE, STUFFING_CHEESE);


///////////////////////////////////////////////////////////////////////////////
console.log('get stuffing ', hamburger.stuffing)
// добавка из майонеза
hamburger.toppings = TOPPING_MAYO;
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// я тут передумал и решил добавить еще приправу
hamburger.toppings = TOPPING_SPICE;
// А сколько теперь стоит? 
console.log("Price with SPICE: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.size === SIZE_LARGE); 
// Убрать добавку
hamburger.removeTopping(TOPPING_SPICE);
// сколько добавок
console.log("Have %d toppings", hamburger.toppings.length); 
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());

// Итоговый гамбургер
console.log(hamburger);
