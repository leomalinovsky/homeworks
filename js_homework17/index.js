let renderCircle = e => {
  e.preventDefault();
  $('.circle-wrapper').remove();
  $('.content').append(
    `<div class="circle-wrapper">
      <div class="circle""></div>
    </div>`);
  let diameterVal = $('.diameter').val();
  $('.circle').css({
    'height':`${diameterVal}px`, 
    'width':`${diameterVal}px`,
    'background': $('.color').val()
  });
}

let showForm = () => {
  $('#initBtn').remove();
  $('.content').append(
    `<form id="form">
      <input type="number" placeholder="диаметр" class="diameter">
      <input type="text" placeholder="цвет" class="color">
      <button>Нарисовать</button>
    </form>`);
  $('#form').submit(renderCircle);
}

$('#initBtn').click(showForm);