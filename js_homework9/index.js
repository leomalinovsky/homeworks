let birthDate = prompt('Введите вашу дату рождения в формате дд.мм.гггг :');

let options = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit'
};

let parseDate = string => {
  let dateArr = string.split(/\D+/g);
  return new Date(Number(dateArr[2]), Number(dateArr[1]-1), Number(dateArr[0]));
}

let getAge = date => {
  let today = new Date();
  let birthDate = parseDate(date);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())){
    age--;
  }
  return age;
}

let getSign = date => {
  let month = parseDate(date).getMonth()+1;
  let day = parseDate(date).getDate();
  switch (month) {
    case 1:
      if (day <= 19)
        sign = 'Козерог';
      else
        sign = 'Водолей';
      break;
    case 2:
      if (day <= 18)
        sign = 'Водолей';
      else
        sign = 'Рыбы';
      break;
    case 3:
      if (day <= 20)
        sign = 'Рыбы';
      else
        sign = 'Овен';
      break;
    case 4:
      if (day <= 19)
        sign = 'Овен';
      else
        sign = 'Телец';
      break;
    case 5:
      if (day <= 20)
        sign = 'Телец';
      else
        sign = 'Близнецы';
      break;
    case 6:
      if (day <= 21)
        sign = 'Близнецы';
      else
        sign = 'Рак';
      break;
    case 7:
      if (day <= 22)
        sign = 'Рак';
      else
        sign = 'Лев';
      break;
    case 8:
      if (day <= 22)
        sign = 'Лев';
      else
        sign = 'Дева';
      break;
    case 9:
      if (day <= 22)
        sign = 'Дева';
      else
        sign = 'Весы';
      break;
    case 10:
      if (day <= 22)
        sign = 'Весы';
      else
        sign = 'Скорпион';
      break;
    case 11:
      if (day <= 22)
        sign = 'Скорпион';
      else
        sign = 'Стрелец';
      break;
    case 12:
      if (day <= 21)
        sign = 'Стрелец';
      else
        sign = 'Козерог';
      break;
  }
  return sign;
}

console.log(`Вам ${getAge(birthDate)} лет!`);
console.log(`Ваш знак зодиака: ${getSign(birthDate)}`);

alert(`Вам ${getAge(birthDate)} лет!`);
alert(`Ваш знак зодиака: ${getSign(birthDate)}`);

