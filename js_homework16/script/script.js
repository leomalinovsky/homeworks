let localValue = localStorage.getItem('theme');

if (localValue === 'normandy') {
	document.querySelector('#theme').href = 'css/theme-normandy.css';
} else {
	document.querySelector('#theme').href = 'css/theme-standart.css';
}

const changeTheme = () => {
	if (localValue === 'standart') {
		localStorage.setItem('theme', 'normandy');
		window.location.reload(); 
	} else {
		localStorage.setItem('theme', 'standart');
		window.location.reload(); 
	}
}

document.querySelector('#themeBtn').addEventListener('click', changeTheme)