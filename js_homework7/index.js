const users = [
	{
	  name: "Ivan",
	  surname: "Ivanov",
	  gender: "male",
	  age: 30
	},
	{
	  name: "Anna",
	  surname: "Ivanova",
	  gender: "female",
	  age: 22
	},
	{
	  name: "Ivan",
	  surname: "Andreev",
	  gender: "male",
	  age: 20
	},
	{
	  name: "Boris",
	  surname: "Vasiliev",
	  gender: "male",
	  age: 32
	},
	{
	  name: "Nikolay",
	  surname: "Ivanov",
	  gender: "male",
	  age: 35
	},
	{
	  name: "Anna",
	  surname: "Petrova",
	  gender: "female",
	  age: 25
	}
];

const excluded = [
	{
	  name: "Ivan",
	  surname: "Ivanov",
	  gender: "male",
	  age: 30
	},
	{
	  name: "Anna",
	  surname: "Petrova",
	  gender: "female",
	  age: 25
	},
	{
	  name: "Boris",
	  surname: "Ivanov",
	  gender: "male",
	  age: 32
	},
];

let excludeBy = (list, excl, value) => {
	let indexArr = []
	excl.map(val => {
		 list.map((v, i) => {
			if (v[value] == val[value]){
				indexArr.push(i);
			}
		});
	});
	return list.filter( (v, i) => {
		return !indexArr.includes(i)
	});
}

console.log(excludeBy(users, excluded, 'name'));