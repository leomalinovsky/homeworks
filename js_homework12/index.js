let renderCircle = e => {
  e.preventDefault();
  let circleWrapper = document.createElement('div');
  let circle = document.createElement('div');
  circle.className = 'circle';
  let diameterVal = document.querySelector('.diameter').value;
  circle.style.height = `${diameterVal}px`;
  circle.style.width = `${diameterVal}px`;
  circle.style.backgroundColor = document.querySelector('.color').value;
  circleWrapper.append(circle);
  document.querySelector('.circle-wrapper').innerHTML = circleWrapper.innerHTML;
}

let showForm = () => {
  let wrapper = document.createElement('div');
  let form = document.createElement('form');
  form.id = 'form'
  let diameter = document.createElement('input');
  let color = document.createElement('input');
  let button = document.createElement('button');
  let circleWrapper = document.createElement('div');
  circleWrapper.className = 'circle-wrapper'
  button.innerHTML = 'Нарисовать';
  diameter.type = 'number';
  color.type = 'text';
  diameter.placeholder = 'диаметр';
  diameter.className = 'diameter'
  color.placeholder = 'цвет';
  color.className = 'color'
  form.append(diameter);
  form.append(color);
  form.append(button);
  wrapper.append(form);
  wrapper.append(circleWrapper);
  document.querySelector('.content').innerHTML = wrapper.innerHTML;
  
  document.querySelector('#form').addEventListener('submit', renderCircle);
}

document.querySelector('.initBtn').addEventListener('click', showForm);