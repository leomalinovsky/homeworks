// <<<<<<<<<< Вариант с использованием шаблонных строк и функции map >>>>>>>>>>

// let numberOfPoints = Number(prompt('Enter the number of points. Integers only!'));

// let listMaker = num => {
// 	let arr = [];
//  	for (let i = 0; i < num; i++) {
//     let pointVal = prompt(`Enter point ${i+1}`);
// 		arr.push(pointVal);
//   }
//   let ul = document.createElement('ul');
//   arr.map( v => {
//   	let li = document.createElement('li');
//   	li.innerHTML = v;
//   	ul.appendChild(li)
//   })
//   document.getElementById('list').appendChild(ul);
// }
// listMaker(numberOfPoints);

// <<<<<<<<<< Вариант (более рациональный на мой взгляд) без использования функции map >>>>>>>>>>
let listMaker = () => {
	let numberOfPoints = Number(prompt('Enter the number of points. Only integers!'));
  let ul = document.createElement('ul');
 	for (let i = 0; i < numberOfPoints; i++) {
  	let li = document.createElement('li');
    let pointVal = prompt(`Enter point ${i+1}`);
  	li.innerHTML = pointVal;
  	ul.appendChild(li)
  }
  document.getElementById('list').appendChild(ul);
}
listMaker();

// <<<<<<<<<< Таймер >>>>>>>>>>

let timer = () => {
	let time = Number(prompt('Set the time (in seconds ) after which the list will be removed. Integers only!'));
	document.getElementById('timer').innerHTML = time;
	let interval = setInterval( () => {
	  document.getElementById('timer').innerHTML = --time;
		if(time == 0) {
	    clearInterval(interval);
	  }
	}, 1000);
	setTimeout( () => {
		document.getElementById('list').innerHTML = 'the time is out';
	}, time*1000);
}
timer();
