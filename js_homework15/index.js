const setValue = (classname, val) => {
  const elem = document.querySelector(classname);
  elem.innerHTML = val;
}

const setMinute = (val) => {
  setValue('.minute', val);
}

const setSecond = (val) => {
  setValue('.second', val);
}

const setMillisecond = (val) => {
  setValue('.millisecond', val);
}

let minute = 0;
let second = 0;
let millisecond = 0;
let interval;

const btnStart = document.querySelector('.start');
const btnStop = document.querySelector('.stop');
const btnClear = document.querySelector('.clear');

setMinute(minute);
setSecond(second);
setMillisecond(millisecond);

const clear = () => {
  btnStart.classList.remove("hide");
  btnStop.classList.add("hide");
  
  clearInterval(interval);
  
  minute = 0;
  second = 0;
  millisecond = 0;
  
  setMinute(minute);
  setSecond(second);
  setMillisecond(millisecond);
}

const pause = () => {
  btnStart.classList.remove("hide");
  btnStop.classList.add("hide");
  clearInterval(interval);
}

const start = () => {
  btnStart.classList.add("hide");
  btnStop.classList.remove("hide");

  interval = setInterval(() => {
    if(millisecond >= 1000) {
      second++;
      millisecond = 0;
      setSecond(second);
    } else {
      millisecond = millisecond + 4;
    }

    if(second >= 60) {
      minute++;
      second = 0;
      setSecond(second);
      setMinute(minute);
    }

    setMillisecond(millisecond);
  }, 4);
}

btnStart.addEventListener('click', start);

btnStop.addEventListener('click', pause);

btnClear.addEventListener('click', clear);
