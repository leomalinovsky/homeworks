let table = document.createElement('table');
let tbody = document.createElement('tbody');

for (let i = 0; i < 30; i++) {
  let tr = document.createElement('tr');
  tbody.appendChild(tr);
  for (let i = 0; i < 30; i++) {
    let td = document.createElement('td');
    tr.appendChild(td);
    td.classList.add('white')
  }
}
table.appendChild(tbody);
document.body.appendChild(table);

document.body.addEventListener('click', e => {
  if (e.target.className == 'white') {
    e.target.className = 'black';
  } else if (e.target.className == 'black') {
    e.target.className = 'white';
  } else if (e.target == document.body) {
    table.classList.toggle('inversion')
  }
})