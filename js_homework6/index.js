let createNewUser = () => {
	let firstName = prompt('Enter your first name');
	let lastName = prompt('Enter your last name');
	return {
		firstName,
		lastName,
		getLogin: () => {
			return firstName.charAt(0).toLowerCase()+lastName.toLowerCase();
		}
	}
}
let user = createNewUser();

console.log(user);
console.log(user.getLogin());