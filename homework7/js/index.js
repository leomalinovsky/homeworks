window.onscroll = function() {
  let scrolled = window.pageYOffset || document.documentElement.scrollTop;
	let fromLeftToRight = (scrolled*4);

  let subtitle = document.getElementById('subtitleWidth');
  let makeSmartDesignParagraph = document.getElementById('makeSmartDesign');
  let make = document.getElementById('make');
  let smart = document.getElementById('smart');
  let design = document.getElementById('design');
	// console.log(fromLeftToRight);
	function scrollToRight (element, triggerPoint) {
  	if (fromLeftToRight >= triggerPoint) {
  		return element.setAttribute("style", "display: inline-block;");
  	} else {
  		return element.setAttribute("style", "display: none;");
  	}	
	}

  if (scrolled >= 0) {
  	subtitle.setAttribute("style", "width:"+ fromLeftToRight +"px;");
		scrollToRight(makeSmartDesignParagraph, 230);
		scrollToRight(make, 400);
		scrollToRight(smart, 620);
		scrollToRight(design, 860);
	}

  let elem = document.getElementById('aboutSection');
  let elem2 = document.getElementById('parallax2Section');
  let elem3 = document.getElementById('teamSection');
  let elem4 = document.getElementById('parallax3Section');
  let elem5 = document.getElementById('workSection');
  let elem6 = document.getElementById('parallax4Section');
  // console.log(scrolled);
  function scrollToBottom (element, triggerPoint) {
		 if (scrolled >= triggerPoint) {
	  	return element.classList.add('visible');
	  } else {
	  	return element.classList.remove('visible');
	  }
  }
  scrollToBottom(elem, 500);
  scrollToBottom(elem2, 750);
  scrollToBottom(elem3, 1200);
  scrollToBottom(elem4, 1400);
  scrollToBottom(elem5, 2000);
  scrollToBottom(elem6, 2200);
}