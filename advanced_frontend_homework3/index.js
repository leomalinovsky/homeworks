/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/

function Hamburger(size, stuffing) {
	try {  
		if (arguments.length === 0) {
			throw new HamburgerException('Error! No arguments');
		} else if (arguments.length === 1) {
			throw new HamburgerException('Error! Should be two arguments');
		} else if (typeof size != 'object' || typeof stuffing != 'object') {
			throw new HamburgerException('size or/and stuffing is not an object');
		} else {
			this.size = size;
			this.stuffing = stuffing;
			this.toppings = [];
			return this; 
		}
	}
	catch (err) {
		console.log(err)
	}
} 

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
	value: 'small',
	price: 50,
	calories: 20
}
Hamburger.SIZE_LARGE = {
	value: 'large',
	price: 100,
	calories: 40
}
Hamburger.STUFFING_CHEESE = {
	name: 'cheese',
	price: 10,
	calories: 20
}
Hamburger.STUFFING_SALAD = {
	name: 'salad',
	price: 20,
	calories: 5
}
Hamburger.STUFFING_POTATO = {
	name: 'potato',
	price: 15,
	calories: 10
}
Hamburger.TOPPING_MAYO = {
	name: 'mayonnaise',
	price: 20,
	calories: 5
}
Hamburger.TOPPING_SPICE = {
	name: 'spice',
	price: 15,
	calories: 0
}

var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function (topping) {
	try {
		if (!this.toppings.includes(topping)){	
			this.toppings.push(topping);	
		} else if (this.toppings.includes(topping)) {
			throw new HamburgerException(topping.name+' is already added');
		}
	}
	catch (err) {
		console.log(err)
	}
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
	try {
		if (this.toppings.length === 0){
			throw new HamburgerException('There is no topping'); 
		} else if (this.toppings.includes(topping)) {
			this.toppings = this.toppings.filter(function(item){
				return item.name != topping.name;
			})
		}
	}
	catch (err) {
		console.log(err)
	}
}
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
	try {
		return this.toppings;
	}
	catch (err) {
		console.log(err)
	}
}
/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
	try {
		return this.size
	}
	catch (err) {
		console.log(err)
	}
}
/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
	try {
		return this.stuffing;
	}
	catch (err) {
		console.log(err)
	}
}
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
	try {
		var price = this.size.price + this.stuffing.price;
		this.toppings.forEach( function (item) {
			price += item.price;
		})
		return price;  
	}
	catch (err) {
		console.log(err)
	}
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
	try {
		var calories = this.size.calories + this.stuffing.calories;
		this.toppings.forEach( function (item) {
			calories += item.calories;
		})
		return calories;
	}
	catch (err) {
		console.log(err)
	}
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException (message) {
   this.message = message;
   return this;
}

///////////////////////////////////////////////////////////////////////////////

// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); 
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); 

// Итоговый гамбургер
console.log(hamburger);
